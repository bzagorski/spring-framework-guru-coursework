package guru.springframework.controller;

import guru.springframework.service.ImageService;
import guru.springframework.service.RecipeService;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@Controller
@RequiredArgsConstructor
public class ImageController {

    private final RecipeService recipeService;
    private final ImageService imageService;

    @GetMapping("/recipes/{recipeId}/images")
    public String showUploadForm(@PathVariable String recipeId, Model model) {
        model.addAttribute("recipe", recipeService.findCommandById(recipeId));

        return "recipe/imageuploadform";
    }

    @PostMapping("/recipes/{recipeId}/images")
    public String handleImagePost(@PathVariable String recipeId, @RequestParam("imageFile") MultipartFile imageFile) {

        imageService.saveImageFile(recipeId, imageFile);

        return "redirect:/recipes/" + recipeId;
    }

    @GetMapping("/recipes/{recipeId}/recipeImage")
    public void renderImage(@PathVariable String recipeId, HttpServletResponse response) throws IOException {
        var recipe = recipeService.findCommandById(recipeId);
        if (recipe != null && recipe.getImage() != null) {
            var byteArray = new byte[recipe.getImage().length];

            int i = 0;
            for (Byte wrappedByte : recipe.getImage()) {
                byteArray[i++] = wrappedByte;
            }

            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            var inputStream = new ByteArrayInputStream(byteArray);
            IOUtils.copy(inputStream, response.getOutputStream());
        }
    }
}
