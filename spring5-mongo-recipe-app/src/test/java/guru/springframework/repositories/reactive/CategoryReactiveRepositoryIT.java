package guru.springframework.repositories.reactive;

import guru.springframework.domain.Category;
import guru.springframework.domain.Recipe;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@DataMongoTest
public class CategoryReactiveRepositoryIT {

    @Autowired
    private CategoryReactiveRepository categoryReactiveRepository;

    @Before
    public void setup() {
        categoryReactiveRepository.deleteAll().block();
    }

    @Test
    public void shouldSave() {
        Category category = new Category();
        category.setDescription("Soup");

        categoryReactiveRepository.save(category).block();

        assertThat(categoryReactiveRepository.count().block())
                .isGreaterThan(0)
                .isEqualTo(1L);
    }

    @Test
    public void shouldFindByDescription() {
        Category category = new Category();
        category.setDescription("Soup");
        categoryReactiveRepository.save(category).block();

        Category result = categoryReactiveRepository.findByDescription("Soup").block();

        assertThat(result)
                .isNotNull()
                .extracting("description")
                .contains("Soup");
    }

}