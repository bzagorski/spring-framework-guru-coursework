package zagsoft.springframework.myspringpetclinic.service.map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import zagsoft.springframework.myspringpetclinic.model.Visit;
import zagsoft.springframework.myspringpetclinic.service.VisitService;

import java.util.Set;

@Service
@Profile({"default", "map"})
public class VisitMapService extends AbstractMapService<Visit, Long> implements VisitService {

    @Override
    public Set<Visit> findAll() {
        return super.findAll();
    }

    @Override
    public Visit findById(Long id) {
        return super.findById(id);
    }

    @Override
    public Visit findByPetId(Long petId) {
        return null;
    }

    @Override
    public Visit save(Visit visit) {
        if (validateVisit(visit)) {
            return super.save(visit);
        } else {
            return null;
        }
    }

    @Override
    public void delete(Visit visit) {
        super.delete(visit);
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    private boolean validateVisit(Visit visit) {
        if (visit == null
                || visit.getPet() == null
                || visit.getPet().getId() == null
                || visit.getPet().getOwner() == null
                || visit.getPet().getOwner().getId() == null
        ) {
            throw new RuntimeException("Invalid visit");
        } else {
            return true;
        }
    }
}
