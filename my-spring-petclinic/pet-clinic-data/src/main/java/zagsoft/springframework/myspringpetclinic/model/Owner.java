package zagsoft.springframework.myspringpetclinic.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "owners")
public class Owner extends Person {

    private String address;

    private String city;

    private String telephone;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
    private Set<Pet> pets = new HashSet<>();

    public Owner() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    /**
     * Return the Pet with the given name, or null if none found for this Owner.
     * @param name to test
     * @return true if pet name is already in use
     */
    public Pet getPet(String name) {
        return getPet(name, false);
    }

    /**
     * Return the Pet with the given name, or null if none found for this Owner.
     * @param name to test
     * @return true if pet name is already in use
     */
    public Pet getPet(String name, boolean ignoreNew) {
        name = name.toLowerCase();
        for (Pet pet : pets) {
            if (!ignoreNew || !pet.isNew()) {
                String compName = pet.getName();
                compName = compName.toLowerCase();
                if (compName.equals(name)) {
                    return pet;
                }
            }
        }
        return null;
    }

    private Owner(OwnerBuilder builder) {
        setId(builder.id);
        setFirstName(builder.firstName);
        setLastName(builder.lastName);
        setAddress(builder.address);
        setCity(builder.city);
        setTelephone(builder.telephone);
        setPets(builder.pets);
    }

    public static OwnerBuilder builder() {
        return new OwnerBuilder();
    }

    public static final class OwnerBuilder {
        private Long id;
        private String firstName;
        private String lastName;
        private String address;
        private String city;
        private String telephone;
        private Set<Pet> pets = new HashSet<>();

        public OwnerBuilder() {
        }

        public OwnerBuilder id(Long val) {
            id = val;
            return this;
        }

        public OwnerBuilder firstName(String val) {
            firstName = val;
            return this;
        }

        public OwnerBuilder lastName(String val) {
            lastName = val;
            return this;
        }

        public OwnerBuilder address(String address) {
            this.address = address;
            return this;
        }

        public OwnerBuilder city(String city) {
            this.city = city;
            return this;
        }

        public OwnerBuilder telephone(String telephone) {
            this.telephone = telephone;
            return this;
        }

        public OwnerBuilder pets(Set<Pet> pets) {
            this.pets = pets;
            return this;
        }

        public Owner build() {
            return new Owner(this);
        }
    }
}
