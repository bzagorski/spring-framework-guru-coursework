package guru.springframework.service;

import org.springframework.web.multipart.MultipartFile;

public interface ImageService {

    void saveImageFile(long anyLong, MultipartFile file);

}
