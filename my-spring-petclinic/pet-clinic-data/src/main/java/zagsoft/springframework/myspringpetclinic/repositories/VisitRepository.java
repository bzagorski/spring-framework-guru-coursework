package zagsoft.springframework.myspringpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import zagsoft.springframework.myspringpetclinic.model.Pet;
import zagsoft.springframework.myspringpetclinic.model.Visit;

import java.util.Optional;

@Repository
public interface VisitRepository extends CrudRepository<Visit, Long> {

    Optional<Visit> findByPet(Pet pet);

    Optional<Visit> findByPetId(Long petId);

}
