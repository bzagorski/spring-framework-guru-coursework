package guru.springframework.converters;

import guru.springframework.commands.UnitOfMeasureCommand;
import guru.springframework.model.Ingredient;
import guru.springframework.model.UnitOfMeasure;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

class IngredientToIngredientCommandTest {

    private static final String DESCRIPTION = "Tacos";
    private static final BigDecimal AMOUNT = BigDecimal.ZERO;
    private static final Long UOM_ID = 2L;
    private static final Long ID_VALUE = 1L;

    private IngredientToIngredientCommand converter;

    @BeforeEach
    void setup() {
        converter = new IngredientToIngredientCommand(new UnitOfMeasureToUnitOfMeasureCommand());
    }

    @Test
    void testNullObject() {
        assertThatCode(() -> converter.convert(null))
                .doesNotThrowAnyException();

        assertThat(converter.convert(null))
                .isNull();
    }

    @Test
    void testEmptyObject() {
        assertThatCode(() -> converter.convert(new Ingredient()))
                .doesNotThrowAnyException();

        assertThat(converter.convert(new Ingredient()))
                .isNotNull();
    }

    @Test
    void testConvertWithNullUom() {
        var ingredient = new Ingredient(DESCRIPTION, AMOUNT, null);
        ingredient.setId(ID_VALUE);

        assertThat(converter.convert(ingredient))
                .isNotNull()
                .extracting("id", "description", "amount")
                .containsExactly(ID_VALUE, DESCRIPTION, AMOUNT);
    }

    @Test
    void testConvertWithUom() {
        var uom = new UnitOfMeasure("cup");
        uom.setId(UOM_ID);
        var expectedUomCommand = new UnitOfMeasureCommand();
        expectedUomCommand.setId(UOM_ID);
        expectedUomCommand.setDescription("cup");
        var ingredient = new Ingredient(DESCRIPTION, AMOUNT, uom);
        ingredient.setId(ID_VALUE);

        assertThat(converter.convert(ingredient))
                .isNotNull()
                .extracting("id", "description", "amount", "unitOfMeasure")
                .containsExactly(ID_VALUE, DESCRIPTION, AMOUNT, expectedUomCommand);
    }
}