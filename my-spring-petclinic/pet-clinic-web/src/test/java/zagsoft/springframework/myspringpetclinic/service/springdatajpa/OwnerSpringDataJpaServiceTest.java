package zagsoft.springframework.myspringpetclinic.service.springdatajpa;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import zagsoft.springframework.myspringpetclinic.model.Owner;
import zagsoft.springframework.myspringpetclinic.repositories.OwnerRepository;

import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

@ExtendWith(MockitoExtension.class)
class OwnerSpringDataJpaServiceTest {

    @Mock
    private OwnerRepository ownerRepository;

    @InjectMocks
    private OwnerSpringDataJpaService ownerService;

    @Test
    void findByLastName() {
        var lastName = "Lebowski";
        var owner = Owner.builder().lastName(lastName).build();
        Mockito.when(ownerRepository.findByLastName(lastName)).thenReturn(Optional.of(owner));

        var result = ownerService.findByLastName(lastName);

        assertThat(result)
                .isNotNull()
                .extracting("lastName")
                .isEqualTo(lastName);
    }

    @Test
    void findAll() {
        var owner1 = Owner.builder().id(1L).lastName("Lebowski").build();
        var owner2 = Owner.builder().id(2L).lastName("Sobchak").build();
        var owners = Set.of(owner1, owner2);
        Mockito.when(ownerRepository.findAll()).thenReturn(owners);

        var result = ownerService.findAll();

        assertThat(result).hasSize(2)
                .extracting("lastName")
                .contains("Lebowski", "Sobchak");
    }

    @Test
    void findById() {
        var ownerId = 1L;
        var owner = Owner.builder().id(ownerId).build();
        Mockito.when(ownerRepository.findById(ownerId)).thenReturn(Optional.of(owner));

        var result = ownerService.findById(ownerId);

        assertThat(result)
                .isNotNull()
                .extracting("id")
                .isEqualTo(ownerId);
    }

    @Test
    void save() {
        var owner = Owner.builder().firstName("Jeff").lastName("Lebowski")
                .build();
        Mockito.when(ownerRepository.save(owner)).thenReturn(owner);

        var result = ownerService.save(owner);

        assertThat(result).isEqualTo(owner);
    }

    @Test
    void delete() {
        var owner = Owner.builder().firstName("Test")
                .build();

        ownerService.delete(owner);

        Mockito.verify(ownerRepository).delete(any());
    }

    @Test
    void deleteById() {
        var owner = Owner.builder().id(1L)
                .build();

        ownerService.deleteById(owner.getId());

        Mockito.verify(ownerRepository).deleteById(anyLong());
    }
}