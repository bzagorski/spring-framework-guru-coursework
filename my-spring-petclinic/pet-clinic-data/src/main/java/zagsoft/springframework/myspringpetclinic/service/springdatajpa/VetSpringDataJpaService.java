package zagsoft.springframework.myspringpetclinic.service.springdatajpa;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import zagsoft.springframework.myspringpetclinic.model.Vet;
import zagsoft.springframework.myspringpetclinic.repositories.VetRepository;
import zagsoft.springframework.myspringpetclinic.service.VetService;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springDataJpa")
public class VetSpringDataJpaService implements VetService {

    private final VetRepository vetRepository;

    public VetSpringDataJpaService(VetRepository vetRepository) {
        this.vetRepository = vetRepository;
    }

    @Override
    public Vet findByLastName(String lastName) {
        return vetRepository.findByLastName(lastName)
                .orElse(null);
    }

    @Override
    public Vet findById(Long id) {
        return vetRepository.findById(id)
                .orElse(null);
    }

    @Override
    public Vet save(Vet vet) {
        return vetRepository.save(vet);
    }

    @Override
    public void delete(Vet vet) {
        vetRepository.delete(vet);
    }

    @Override
    public void deleteById(Long id) {
        vetRepository.deleteById(id);
    }

    @Override
    public Set<Vet> findAll() {
        Set<Vet> vets = new HashSet<>();
        vetRepository.findAll().forEach(vets::add);
        return vets;
    }
}
