package guru.springframework.repository;

import guru.springframework.model.UnitOfMeasure;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataMongoTest
class UnitOfMeasureRepositoryIT {

    @Autowired
    private UnitOfMeasureRepository unitOfMeasureRepository;

    @Test
    void findByDescriptionCup() {
        var uom = new UnitOfMeasure("cup");
        unitOfMeasureRepository.save(uom);

        var cup = unitOfMeasureRepository.findByDescription("cup");

        assertThat(cup.isPresent()).isTrue();
        assertThat(cup.get().getDescription()).isEqualTo("cup");
    }
}