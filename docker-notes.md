# Docker Notes

### Getting up and running with a MongoDB Docker Container

###### Download docker image
```bash
docker run -d mongo
```
* Expose 27017 to use the mongodb's 27017 port. This will allow other applications running on your local container to access the database

```bash
docker run -p 27017:27017 -d mongo
```

* View logs for container:
```bash
docker logs -f <containerId>
```

* Inspect a docker image:
```bash
docker image inspect <container>
```

* Image ids of the image are derived from the layers. If the layers change then the hash will change.
* Docker images are composed of multiple layers. All but the last layer are immutable. The last image can be compressed and otherwise modified.

#### Docker Non Persistent Storage
* State is not saved when the container is stopped
* The last layer is destroyed when you stop the container. A new layer is created when you start the image again.
* The layer that is stopped remains on the file system, and these will take up space. You can clean these up.

#### Docker Persistent Storage
```bash
mkdir ~/data/mongo
docker run -p 27017:27017 -v ~/data/mongo -d mongo

#### Docker RabbitMQ
```bash
docker run -d --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```
* 5672 is the port exposed for client connections
* 15672 is for the RabbitMQ management website

#### Docker MySQL
```bash
# setup the mysql image
docker run -d --name zag-mysql -e MYSQL_ROOT_PASSWORD=manager -v /home/dev/data/mysql:/var/lib/mysql mysql
```

#### Docker Cleanup
* Kill all running containers
```bash
docker kill $(docker ps -q)
```
* Delete all stopped containers
```bash
docker rum $(docker ps -a -q)
```
* Delete Image
```bash
docker rmi <image name>
```
* Delete untagged (dangling) images
```bash
docker rmi $(docker images -q -f dangling=true)
```
* Delete all images
```bash
docker rmi $(docker images -q)
```
* Remove all dangling volumes
```bash
docker volume rm $(docker volume ls -f dangling=true -q)
```
* Note this will not remove files from the host system in shared volumes


