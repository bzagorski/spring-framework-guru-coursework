package guru.springframework.controller;

import guru.springframework.commands.RecipeCommand;
import guru.springframework.service.ImageService;
import guru.springframework.service.RecipeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class ImageControllerTest {

    @Mock
    private ImageService imageService;

    @Mock
    private RecipeService recipeService;

    @InjectMocks
    private ImageController imageController;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(imageController)
                .setControllerAdvice(ControllerExceptionHandler.class).build();
    }

    @Test
    @DisplayName("GET /recipes/{recipeId}/images - SUCCESS")
    void getImageForm() throws Exception {
        var recipeCommand = new RecipeCommand();
        recipeCommand.setId(1L);

        when(recipeService.findCommandById(anyLong()))
                .thenReturn(recipeCommand);

        mockMvc.perform(get("/recipes/{recipeId}/images", 1L))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("recipe"));

        verify(recipeService).findCommandById(1L);
    }

    @Test
    @DisplayName("POST /recipes/{recipeId}/images - SUCCESS")
    void handleImagePost() throws Exception {
        var file = new MockMultipartFile("imageFile", "testing.txt", "text/plain", "Spring Framework Guru".getBytes());

        mockMvc.perform(multipart("/recipes/{recipeId}/images", 1L).file(file))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/recipes/1"));

        verify(imageService).saveImageFile(anyLong(), any());
    }

    @Test
    @DisplayName("GET /recipes/{recipeId}/recipeImage")
    void renderImageFromDb() throws Exception {
        var recipe = new RecipeCommand();
        recipe.setId(1L);

        var imageText = "test-image-text";
        var bytes = new Byte[imageText.getBytes().length];

        int i = 0;
        for (byte b : imageText.getBytes()) {
            bytes[i++] = b;
        }
        recipe.setImage(bytes);

        when(recipeService.findCommandById(anyLong()))
                .thenReturn(recipe);

        var response = mockMvc.perform(get("/recipes/{recipeId}/recipeImage", 1L))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        var responseBytes = response.getContentAsByteArray();
        assertThat(responseBytes).isEqualTo(imageText.getBytes());
    }

    @Test
    @DisplayName("Bad Request")
    void shouldHandleBadRequest() throws Exception {
        mockMvc.perform(get("/recipes/asdf/recipeImage"))
                .andExpect(status().isBadRequest())
                .andExpect(view().name("400error"));
    }
}