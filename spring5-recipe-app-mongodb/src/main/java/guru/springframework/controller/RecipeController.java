package guru.springframework.controller;

import guru.springframework.commands.RecipeCommand;
import guru.springframework.exception.NotFoundException;
import guru.springframework.service.RecipeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequiredArgsConstructor
public class RecipeController {

    private final RecipeService recipeService;

    @GetMapping("/recipes/{id}")
    public String showRecipe(@PathVariable String id, Model model) {
        var recipe = recipeService.findRecipeById(id);

        model.addAttribute("recipe", recipe);

        return "recipe/show";
    }

    @GetMapping("recipes/new")
    public String newRecipe(Model model) {
        model.addAttribute("recipe", new RecipeCommand());

        return "recipe/recipeform";
    }

    @PostMapping("recipes")
    public String saveOrUpdate(@ModelAttribute("recipe") RecipeCommand recipeCommand, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors()
                    .forEach(error -> log.debug(error.toString()));
            return "recipe/recipeform";
        }
        var saved = recipeService.saveRecipeCommand(recipeCommand);

        return "redirect:/recipes/" + saved.getId();
    }

    @GetMapping("/recipes/{id}/update")
    public String updateRecipe(@PathVariable String id, Model model) {
        model.addAttribute("recipe", recipeService.findCommandById(id));

        return "recipe/recipeform";
    }

    @GetMapping("/recipes/{id}/delete")
    public String deleteRecipe(@PathVariable String id) {
        recipeService.deleteById(id);

        return "redirect:/";
    }

    @ResponseStatus(HttpStatus.NOT_FOUND) // Exception handler takes precedence over the exception class
    @ExceptionHandler(NotFoundException.class)
    public ModelAndView handleNotFound(NotFoundException ex) {
        log.error("Handling not found exception");

        var model = new ModelAndView("404error");
        model.addObject("message", ex.getMessage());
        return model;
    }

}
