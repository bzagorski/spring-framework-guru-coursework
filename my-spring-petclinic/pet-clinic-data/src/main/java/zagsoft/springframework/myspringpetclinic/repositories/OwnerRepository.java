package zagsoft.springframework.myspringpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import zagsoft.springframework.myspringpetclinic.model.Owner;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface OwnerRepository extends CrudRepository<Owner, Long> {

    Optional<Owner> findByLastName(String lastName);

    List<Owner> findAllByLastNameLikeIgnoreCase(String lastName);
}
