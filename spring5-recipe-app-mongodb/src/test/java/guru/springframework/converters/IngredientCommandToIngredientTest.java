package guru.springframework.converters;

import guru.springframework.commands.IngredientCommand;
import guru.springframework.commands.UnitOfMeasureCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

class IngredientCommandToIngredientTest {

    public static final BigDecimal AMOUNT = new BigDecimal("1");
    public static final String DESCRIPTION = "Cheeseburger";
    public static final String ID_VALUE = "1L";
    public static final String UOM_ID = "2L";

    private IngredientCommandToIngredient converter;

    @BeforeEach
    void setUp() {
        converter = new IngredientCommandToIngredient(new UnitOfMeasureCommandToUnitOfMeasure());
    }

    @Test
    void testNullObject() {
        assertThat(converter.convert(null)).isNull();
        assertThatCode(() -> converter.convert(null))
                .doesNotThrowAnyException();
    }

    @Test
    void testEmptyObject() {
        assertThatCode(() -> converter.convert(new IngredientCommand()))
                .doesNotThrowAnyException();
        assertThat(converter.convert(new IngredientCommand()))
                .isNotNull();
    }

    @Test
    void convert() {
        var command = new IngredientCommand();
        command.setId(ID_VALUE);
        command.setAmount(AMOUNT);
        command.setDescription(DESCRIPTION);
        var uomCommand = new UnitOfMeasureCommand();
        uomCommand.setId(UOM_ID);
        command.setUnitOfMeasure(uomCommand);

        var ingredient = converter.convert(command);

        assertThat(ingredient)
                .isNotNull()
                .extracting("id", "amount", "description", "unitOfMeasure.id")
                .containsExactly(ID_VALUE, AMOUNT, DESCRIPTION, UOM_ID);
    }

    @Test
    void convertWithNullUom() {
        var command = new IngredientCommand();
        command.setId(ID_VALUE);
        command.setAmount(AMOUNT);
        command.setDescription(DESCRIPTION);

        var ingredient = converter.convert(command);

        assertThat(ingredient)
                .isNotNull()
                .extracting("id", "amount", "description", "unitOfMeasure")
                .containsExactly(ID_VALUE, AMOUNT, DESCRIPTION, null);
    }

}