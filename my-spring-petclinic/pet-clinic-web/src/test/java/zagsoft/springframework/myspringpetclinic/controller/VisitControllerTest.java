package zagsoft.springframework.myspringpetclinic.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import zagsoft.springframework.myspringpetclinic.model.Owner;
import zagsoft.springframework.myspringpetclinic.model.Pet;
import zagsoft.springframework.myspringpetclinic.model.PetType;
import zagsoft.springframework.myspringpetclinic.model.Visit;
import zagsoft.springframework.myspringpetclinic.service.PetService;
import zagsoft.springframework.myspringpetclinic.service.VisitService;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class VisitControllerTest {

    @Mock
    private PetService petService;

    @Mock
    private VisitService visitService;

    @InjectMocks
    private VisitController visitController;

    private MockMvc mockMvc;

    private Pet pet;

    private Visit visit;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(visitController).build();
        Long petId = 1L;
        Long ownerId = 1L;
        when(petService.findById(anyLong()))
                .thenReturn(
                        Pet.PetBuilder.aPet()
                                .id(petId)
                                .birthDate(LocalDate.of(2018,11,13))
                                .name("Cutie")
                                .owner(Owner.builder()
                                        .id(ownerId)
                                        .lastName("Doe")
                                        .firstName("Joe")
                                        .build())
                                .petType(new PetType("Fish"))
                                .build()
                );
    }

    @Test
    void verifyInitCreationForm() throws Exception {
        mockMvc.perform(get("/owners/*/pets/{petId}/visits/new", 1L))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("pet"))
                .andExpect(view().name("pets/createOrUpdateVisitForm"));
    }

    @Test
    void verifyCreationFormProcessing() throws Exception {
        mockMvc.perform(post("/owners/{ownerId}/pets/{petId}/visits/new", 1L, 1L)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("date", "2020-08-20")
                .param("description", "visit description"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/owners/{ownerId}"));

        verify(visitService).save(any());
    }

}