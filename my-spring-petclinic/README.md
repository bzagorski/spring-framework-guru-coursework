# Custom Spring Petclinic Project
[![pipeline status](https://gitlab.com/the_Dude_Abides/my-spring-petclinic/badges/master/pipeline.svg)](https://gitlab.com/the_Dude_Abides/my-spring-petclinic/-/commits/master)

### Resources
* [Dependency Injection Examples](https://github.com/springframeworkguru/sfg-di)


### OOP Principles

###### Single Responsibility Principle
* A class should have a single responsibility and do accomplish it well
* There should only be one reason for a class to change
* Your classes should be small, no more than a screen full of code
* Avoid "God Classes"
* Split big classes into small classes
* This is a frequently violated principle
* Ensures that tests can be easily tested

###### Open Closed Principle
* Open for extension
* Closed for modification
* You should be able to extend a class's behavior without modifying it
* Use private variables for getters and setters and ONLY when you need them
* Use abstract base classes
> “Open for extension “: This means that the behavior of a software module, say a class can be extended to make it behave in new and different ways. It is important to note here that the term “extended ” is not limited to inheritance using the Java extend keyword. As mentioned earlier, Java did not exist at that time. What it means here is that a module should provide extension points to alter its behavior. One way is to make use of polymorphism to invoke extended behaviors of an object at run time.
> “Closed for modification “: This means that the source code of such a module remains unchanged.
* The answer in Java is abstraction. You can create abstractions (Java interfaces and abstract classes) that are fixed and yet represent an unbounded group of possible behaviors through concrete subclasses.

###### Liskov Substitution Principle
* Barbara Liskov 1998
* Objects in a program would be replaceable with instances of their subtypes without altering the correctness of the program
* Violations will often fail the "is a" test

###### Interface Segregation Principle
* Make fine-grained interfaces that are client specific
* Many client specific interfaces are better than one general purpose
* Keep your components focused and minimize dependencies between them
> What the Interface Segregation Principle says is that your interface should not be bloated with methods that implementing classes don’t require. For such interfaces, also called “fat interfaces”, implementing classes are unnecessarily forced to provide implementations (dummy/empty) even for those methods that they don’t need. In addition, the implementing classes are subject to change when the interface changes. An addition of a method or change to a method signature requires modifying all the implementation classes even if some of them don’t use the method.
> Both the Interface Segregation Principle and Single Responsibility Principle have the same goal: ensuring small, focused, and highly cohesive software components. 


###### Dependency Inversion Principle
* Abstractions should not depend on details 
* Details should depend upon abstractions
* Important that higher level and lower level objects depend on the same abstract interaction

### Dependency Injection
> When you go and get things out of the refrigerator for yourself, you can cause problems. You might leave the door open, you might get something Mommy or Daddy doesn't want you to have. You might even be looking for something we don't even have or which has expired. What you should be doing is stating a need, "I need something to drink with lunch," and then we will make sure you have something when you sit down to eat. John Munsch, 28 October 2009
* Prefer using interfaces for DI
* Avoid DI with concrete classes
* Inversion of Control allows dependencies to be injected at runtime
* Favor constructor injection
* Use final properties
* Code to an interface whenever practical

