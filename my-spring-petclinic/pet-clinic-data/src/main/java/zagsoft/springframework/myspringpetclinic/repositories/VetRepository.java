package zagsoft.springframework.myspringpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import zagsoft.springframework.myspringpetclinic.model.Vet;

import java.util.Optional;

@Repository
public interface VetRepository extends CrudRepository<Vet, Long> {
    Optional<Vet> findByLastName(String lastName);
}
