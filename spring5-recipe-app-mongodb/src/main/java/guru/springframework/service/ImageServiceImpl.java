package guru.springframework.service;

import guru.springframework.repository.RecipeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {

    private final RecipeRepository recipeRepository;

    @Override
    @Transactional
    public void saveImageFile(String recipeId, MultipartFile file) {
        recipeRepository.findById(recipeId)
                .ifPresent(recipe -> {
                    log.info("received file");
                    try {
                        var bytes = new Byte[file.getBytes().length];
                        int i = 0;

                        for (byte b : file.getBytes()) {
                            bytes[i++] = b;
                        }
                        recipe.setImage(bytes);
                        recipeRepository.save(recipe);
                    } catch (IOException e) {
                        log.error("Error while saving recipe image", e);
                    }
                });
    }
}
