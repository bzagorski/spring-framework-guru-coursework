package zagsoft.springframework.myspringpetclinic.model;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "pets")
public class Pet extends BaseEntity {

    private String name;

    @ManyToOne
    @JoinColumn(name = "name_id")
    private PetType petType;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Owner owner;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pet")
    private Set<Visit> visits = new HashSet<>();

    public Pet() {
    }

    public Pet(String name, PetType petType, Owner owner) {
        this.name = name;
        this.petType = petType;
        this.owner = owner;
    }

    public Pet(String name, PetType petType, Owner owner, LocalDate birthDate) {
        this.name = name;
        this.petType = petType;
        this.owner = owner;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PetType getPetType() {
        return petType;
    }

    public void setPetType(PetType petType) {
        this.petType = petType;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Set<Visit> getVisits() {
        return this.visits;
    }

    private void setVisits(Set<Visit> visits) {
        this.visits = visits;
    }

    public static PetBuilder builder() {
        return new PetBuilder();
    }


    public static final class PetBuilder {
        private Pet pet;

        private PetBuilder() {
            pet = new Pet();
        }

        public static PetBuilder aPet() {
            return new PetBuilder();
        }

        public PetBuilder name(String name) {
            pet.setName(name);
            return this;
        }

        public PetBuilder petType(PetType petType) {
            pet.setPetType(petType);
            return this;
        }

        public PetBuilder owner(Owner owner) {
            pet.setOwner(owner);
            return this;
        }

        public PetBuilder birthDate(LocalDate birthDate) {
            pet.setBirthDate(birthDate);
            return this;
        }

        public PetBuilder id(Long id) {
            pet.setId(id);
            return this;
        }

        public PetBuilder visits(Set<Visit> visits) {
            pet.setVisits(visits);
            return this;
        }

        public Pet build() {
            if (CollectionUtils.isEmpty(pet.getVisits())) {
                pet.visits = new HashSet<>();
            }
            return pet;
        }
    }
}
