package guru.springframework.model;

public enum Difficulty {
    EASY, MODERATE, HARD;

    @Override
    public String toString() {
        var str = this.name();
        return str.toLowerCase()
                .substring(0, 1)
                .toUpperCase() + str.substring(1);
    }
}
