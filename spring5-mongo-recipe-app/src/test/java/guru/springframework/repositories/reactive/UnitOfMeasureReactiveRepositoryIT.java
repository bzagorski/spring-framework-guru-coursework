package guru.springframework.repositories.reactive;

import guru.springframework.domain.UnitOfMeasure;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataMongoTest
public class UnitOfMeasureReactiveRepositoryIT {

    @Autowired
    private UnitOfMeasureReactiveRepository unitOfMeasureReactiveRepository;

    @Before
    public void setup() {
        unitOfMeasureReactiveRepository.deleteAll().block();
    }

    @Test
    public void shouldSave() {
        UnitOfMeasure unitOfMeasure = new UnitOfMeasure();
        unitOfMeasure.setDescription("EACH");

        UnitOfMeasure result = unitOfMeasureReactiveRepository.save(unitOfMeasure).block();

        assertThat(result).isNotNull()
                .extracting("description")
                .contains("EACH");
    }

    @Test
    public void shouldFindByDescription() {
        UnitOfMeasure unitOfMeasure = new UnitOfMeasure();
        unitOfMeasure.setDescription("Cup");
        unitOfMeasureReactiveRepository.save(unitOfMeasure).block();

        UnitOfMeasure result = unitOfMeasureReactiveRepository.findByDescription("Cup").block();

        assertThat(result).isNotNull()
                .extracting("description")
                .contains("Cup");

    }
}