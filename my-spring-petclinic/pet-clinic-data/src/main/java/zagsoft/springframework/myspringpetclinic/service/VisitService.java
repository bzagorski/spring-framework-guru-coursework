package zagsoft.springframework.myspringpetclinic.service;

import zagsoft.springframework.myspringpetclinic.model.Visit;

public interface VisitService extends CrudService<Visit, Long> {

    Visit findByPetId(Long petId);
}
