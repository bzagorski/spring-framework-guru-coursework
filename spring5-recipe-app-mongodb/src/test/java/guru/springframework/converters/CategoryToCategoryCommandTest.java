package guru.springframework.converters;

import guru.springframework.model.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

class CategoryToCategoryCommandTest {

    private CategoryToCategoryCommand converter;

    @BeforeEach
    void setUp() {
        converter = new CategoryToCategoryCommand();
    }

    @Test
    void handleNullObject() {
        assertThatCode(() -> converter.convert(null))
                .doesNotThrowAnyException();

        assertThat(converter.convert(null))
                .isNull();
    }

    @Test
    void handleEmptyObject() {
        assertThatCode(() -> converter.convert(new Category()))
                .doesNotThrowAnyException();

        assertThat(converter.convert(new Category()))
                .isNotNull();
    }

    @Test
    void convert() {
        var category = new Category("category description");
        category.setId("1L");

        var result = converter.convert(category);

        assertThat(result)
                .isNotNull()
                .extracting("id", "description")
                .containsExactly(category.getId(), category.getDescription());
    }
}