package guru.springframework.controller;

import guru.springframework.commands.IngredientCommand;
import guru.springframework.commands.UnitOfMeasureCommand;
import guru.springframework.service.IngredientService;
import guru.springframework.service.RecipeService;
import guru.springframework.service.UnitOfMeasureService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Objects;

@Slf4j
@Controller
@RequiredArgsConstructor
public class IngredientController {

    private final RecipeService recipeService;
    private final IngredientService ingredientService;
    private final UnitOfMeasureService unitOfMeasureService;

    @GetMapping("/recipes/{id}/ingredients")
    public String viewRecipeIngredients(@PathVariable String id, Model model) {
        log.debug("Getting ingredient list for recipe: {}", id);
        var recipeCommand = recipeService.findCommandById(id);

        model.addAttribute("recipe", recipeCommand);

        return "recipe/ingredient/list";
    }

    @GetMapping("/recipes/{recipeId}/ingredients/{ingredientId}/show")
    public String viewIngredient(@PathVariable String recipeId, @PathVariable String ingredientId, Model model) {
        log.debug("Viewing recipe: {} ingredient: {}", recipeId, ingredientId);
        var ingredientCommand = ingredientService.findByRecipeIdAndId(recipeId, ingredientId);

        model.addAttribute("ingredient", ingredientCommand);

        return "recipe/ingredient/show";
    }

    @GetMapping("/recipes/{recipeId}/ingredients/{ingredientId}/update")
    public String updateIngredient(@PathVariable String recipeId, @PathVariable String ingredientId, Model model) {
        var ingredient = ingredientService.findByRecipeIdAndId(recipeId, ingredientId);
        model.addAttribute("ingredient", ingredient);
        model.addAttribute("uomList", unitOfMeasureService.listAllUoms());

        return "recipe/ingredient/ingredientform";
    }

    @PostMapping("/recipes/{recipeId}/ingredients")
    public String saveOrUpdate(@PathVariable String recipeId, @ModelAttribute IngredientCommand ingredientCommand) {
        var saved = ingredientService.saveIngredientCommand(ingredientCommand);

        log.debug("Saved recipe id: " + saved.getRecipeId());
        log.debug("Saved ingredient id:" + saved.getId());

        return "redirect:/recipes/" + saved.getRecipeId() + "/ingredients/" + saved.getId() + "/show";
    }

    @GetMapping("/recipes/{recipeId}/ingredients/new")
    public String createNewIngredient(@PathVariable String recipeId, Model model) {
        Objects.requireNonNull(recipeService.findCommandById(recipeId), "Recipe with id: " + recipeId + " not found");

        var ingredient = new IngredientCommand();
        ingredient.setRecipeId(recipeId);
        ingredient.setUnitOfMeasure(new UnitOfMeasureCommand());

        model.addAttribute("ingredient", ingredient);
        model.addAttribute("uomList", unitOfMeasureService.listAllUoms());

        return "recipe/ingredient/ingredientform";
    }

    @GetMapping("/recipes/{recipeId}/ingredients/{ingredientId}/delete")
    public String deleteIngredient(@PathVariable String recipeId, @PathVariable String ingredientId, Model model) {
        ingredientService.deleteById(recipeId, ingredientId);

        return "redirect:/recipes/" + recipeId + "/ingredients";
    }
}
