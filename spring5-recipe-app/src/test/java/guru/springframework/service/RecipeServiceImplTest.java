package guru.springframework.service;

import guru.springframework.exception.NotFoundException;
import guru.springframework.model.Recipe;
import guru.springframework.repository.RecipeRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecipeServiceImplTest {

    @Mock
    private RecipeRepository recipeRepository;

    @InjectMocks
    private RecipeServiceImpl recipeService;

    @Test
    @DisplayName("Find All Recipes")
    void shouldFindAllRecipes() {
        var recipe1 = Recipe.newBuilder().id(1L).build();
        var recipe2 = Recipe.newBuilder().id(2L).build();
        var recipe3 = Recipe.newBuilder().id(2L).build();
        when(recipeRepository.findAll()).thenReturn(List.of(recipe1, recipe2));

        var actual = recipeService.getAllRecipes();

        assertThat(actual).isNotEmpty()
                .isInstanceOf(Set.class)
                .hasSize(2)
                .containsOnly(recipe1, recipe2);
    }

    @Test
    @DisplayName("Recipe - Found")
    void shouldFindRecipeById() {
        var recipe = Recipe.newBuilder().id(1L).build();
        when(recipeRepository.findById(1L)).thenReturn(Optional.of(recipe));

        var actual = recipeService.findRecipeById(1L);

        assertThat(actual).isNotNull()
                .extracting("id")
                .isEqualTo(1L);
    }

    @Test
    @DisplayName("Recipe - Not Found")
    void shouldThrowExceptionWhenRecipeNotFound() {
        when(recipeRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(() -> recipeService.findRecipeById(1L))
                .withMessage("Recipe not found for id: 1");
    }

    @Test
    @DisplayName("Delete Recipe")
    void shouldDeleteRecipe() {
        var id = 1L;
        recipeService.deleteById(id);

        verify(recipeRepository).deleteById(id);
    }

    @Test
    @DisplayName("Handle Exception")
    void shouldHandleException() {
        when(recipeRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatExceptionOfType(NotFoundException.class)
                .isThrownBy(() -> recipeService.findRecipeById(1L));
    }

}