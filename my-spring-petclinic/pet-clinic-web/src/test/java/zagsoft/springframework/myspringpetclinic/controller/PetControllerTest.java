package zagsoft.springframework.myspringpetclinic.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import zagsoft.springframework.myspringpetclinic.model.Owner;
import zagsoft.springframework.myspringpetclinic.model.PetType;
import zagsoft.springframework.myspringpetclinic.service.OwnerService;
import zagsoft.springframework.myspringpetclinic.service.PetService;
import zagsoft.springframework.myspringpetclinic.service.PetTypeService;

import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(MockitoExtension.class)
class PetControllerTest {

    @Mock
    private PetService petService;

    @Mock
    private PetTypeService petTypeService;

    @Mock
    private OwnerService ownerService;

    @InjectMocks
    private PetController petController;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(petController).build();
        Owner owner1 = Owner.builder().id(1L).build();

        when(ownerService.findById(anyLong())).thenReturn(owner1);
        when(petTypeService.findAll()).thenReturn(Set.of(
                new PetType("reptile")));
    }

    @Test
    void initCreationForm() throws Exception {
        mockMvc.perform(get("/owners/{ownerId}/pets/new", 1L))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("owner"))
                .andExpect(model().attributeExists("types"))
                .andExpect(view().name("pets/createOrUpdatePetForm"));
    }

    @Test
    void processCreationForm() throws Exception {
        mockMvc.perform(post("/owners/{ownerId}/pets/new", 1L))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/owners/{ownerId}"));
        verify(petService).save(any());
    }

    @Test
    void initUpdateForm() throws Exception {
        mockMvc.perform(get("/owners/{ownerId}/pets/{petId}/edit", 1L, 2L))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("owner"))
                .andExpect(model().attributeExists("types"))
                .andExpect(view().name("pets/createOrUpdatePetForm"));
    }

    @Test
    void processUpdateForm() throws Exception {
        mockMvc.perform(post("/owners/{ownerId}/pets/{petId}/edit", 1L, 2L))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/owners/{ownerId}"));

        verify(petService).save(any());
    }
}