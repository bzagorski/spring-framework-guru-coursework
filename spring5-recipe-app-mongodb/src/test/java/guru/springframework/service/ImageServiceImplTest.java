package guru.springframework.service;

import guru.springframework.model.Recipe;
import guru.springframework.repository.RecipeRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ImageServiceImplTest {

    @Mock
    private RecipeRepository recipeRepository;

    @InjectMocks
    private ImageServiceImpl imageService;

    @Test
    @DisplayName("Save image file")
    void testSaveImageFile() throws IOException {
        var id = "1";
        var multipartFile = new MockMultipartFile("imageFile", "testing.txt", "text/plain", "Zagsoft".getBytes());
        var recipe = new Recipe();
        recipe.setId(id);

        when(recipeRepository.findById(anyString()))
                .thenReturn(Optional.of(recipe));
        var captor = ArgumentCaptor.forClass(Recipe.class);

        imageService.saveImageFile(id, multipartFile);

        verify(recipeRepository).save(captor.capture());
        var savedRecipe = captor.getValue();
        assertThat(savedRecipe.getImage().length).isEqualTo(multipartFile.getBytes().length);
    }

}