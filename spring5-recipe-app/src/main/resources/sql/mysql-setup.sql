-- Use docker image
-- Connect: docker exec -it dev-mysql bash

CREATE DATABASE sfg_dev;
CREATE DATABASE sfg_prod;

CREATE USER 'sfg_dev'@'localhost' IDENTIFIED BY 'secure';
CREATE USER 'sfg_prod'@'localhost' IDENTIFIED BY 'secure';
CREATE USER 'sfg_dev'@'%' IDENTIFIED BY 'secure';
CREATE USER 'sfg_prod'@'%' IDENTIFIED BY 'secure';

GRANT SELECT ON sfg_dev.* TO 'sfg_dev'@'localhost';
GRANT INSERT ON sfg_dev.* TO 'sfg_dev'@'localhost';
GRANT DELETE ON sfg_dev.* TO 'sfg_dev'@'localhost';
GRANT UPDATE ON sfg_dev.* TO 'sfg_dev'@'localhost';

GRANT SELECT ON sfg_prod.* TO 'sfg_prod'@'localhost';
GRANT INSERT ON sfg_prod.* TO 'sfg_prod'@'localhost';
GRANT DELETE ON sfg_prod.* TO 'sfg_prod'@'localhost';
GRANT UPDATE ON sfg_prod.* TO 'sfg_prod'@'localhost';

GRANT SELECT ON sfg_dev.* TO 'sfg_dev'@'%';
GRANT INSERT ON sfg_dev.* TO 'sfg_dev'@'%';
GRANT DELETE ON sfg_dev.* TO 'sfg_dev'@'%';
GRANT UPDATE ON sfg_dev.* TO 'sfg_dev'@'%';

GRANT SELECT ON sfg_prod.* TO 'sfg_prod'@'%';
GRANT INSERT ON sfg_prod.* TO 'sfg_prod'@'%';
GRANT DELETE ON sfg_prod.* TO 'sfg_prod'@'%';
GRANT UPDATE ON sfg_prod.* TO 'sfg_prod'@'%';

FLUSH PRIVILEGES;