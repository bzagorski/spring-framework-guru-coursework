# Spring Boot Recipe Application

[![CircleCI](https://circleci.com/gh/springframeworkguru/spring5-recipe-app.svg?style=svg)](https://circleci.com/gh/springframeworkguru/spring5-recipe-app)

This repository is for an example application built in my Spring Framework 5 - Beginner to Guru

You can learn about my Spring Framework 5 Online course [here.](https://go.springframework.guru/spring-framework-5-online-course)

### Notes
* Enum persistence - Ordinal vs String
    - DB data that used Ordinal will need to be updated if you add a new enum value
* Hibernate DDL
    - DDL: Data Definition Language
    - Set by spring.jpa.hibernate.ddl-auto
    - Options are: none, validate, update, create, create-drop
    - Spring Boot will use create-drop for embedded databases
* Initializing Data
    - Hibernate will load import.sql if found in the root of the classpath and Hibernate is set to create-drop
        - This is Hibernate specific, not Spring
    - Spring will automatically load schema.sql and data.sql from the root of the classpath
    - Spring will also load schema-${platform}.sql and data-${data}.sql when spring.datasource.platform is set
    - It is recommended that you do one or the other