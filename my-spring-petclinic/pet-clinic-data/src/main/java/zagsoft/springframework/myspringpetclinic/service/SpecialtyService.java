package zagsoft.springframework.myspringpetclinic.service;

import zagsoft.springframework.myspringpetclinic.model.Specialty;

public interface SpecialtyService extends CrudService<Specialty, Long> {

    Specialty findByDescription(String description);
}
