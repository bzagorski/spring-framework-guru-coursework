package zagsoft.springframework.myspringpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import zagsoft.springframework.myspringpetclinic.model.Specialty;

import java.util.Optional;

@Repository
public interface SpecialtyRepository extends CrudRepository<Specialty, Long> {
    Optional<Specialty> findByDescription(String description);
}
