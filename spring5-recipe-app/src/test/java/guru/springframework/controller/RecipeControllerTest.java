package guru.springframework.controller;

import guru.springframework.commands.RecipeCommand;
import guru.springframework.exception.NotFoundException;
import guru.springframework.model.Recipe;
import guru.springframework.service.RecipeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class RecipeControllerTest {

    @Mock
    private RecipeService recipeService;

    @InjectMocks
    private RecipeController recipeController;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(recipeController)
                .setControllerAdvice(ControllerExceptionHandler.class).build();
    }

    @Test
    @DisplayName("GET /recipes/{id} - Recipe Detail")
    void shouldReturnRecipeDetailPage() throws Exception {
        var recipe = Recipe.newBuilder().id(1L).build();
        when(recipeService.findRecipeById(1L)).thenReturn(recipe);

        mockMvc.perform(get("/recipes/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/show"))
                .andExpect(model().attribute("recipe", recipe));
    }

    @Test
    @DisplayName("GET /recipes/{id} - Recipe Not Found")
    void shouldThrowNotFoundResponse() throws Exception {

        when(recipeService.findRecipeById(anyLong()))
                .thenThrow(NotFoundException.class);

        mockMvc.perform(get("/recipes/{id}", 1L))
                .andExpect(status().isNotFound())
                .andExpect(view().name("404error"));
    }

    @Test
    @DisplayName("GET /recipes/{id} - Bad Request")
    void shouldHandleBadRequest() throws Exception {
        mockMvc.perform(get("/recipes/asdf"))
                .andExpect(status().isBadRequest())
                .andExpect(view().name("400error"));
    }

    @Test
    @DisplayName("GET /recipes/new - New Recipe Form")
    void shouldReturnRecipeForm() throws Exception {
        mockMvc.perform(get("/recipes/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/recipeform"))
                .andExpect(model().attribute("recipe", new RecipeCommand()));
    }

    @Test
    @DisplayName("POST /recipes - Save or Update Recipe")
    void shouldSaveOrUpdateRecipe() throws Exception {
        var recipeCommand = new RecipeCommand();
        recipeCommand.setId(1L);
        when(recipeService.saveRecipeCommand(any()))
                .thenReturn(recipeCommand);

        mockMvc.perform((post("/recipes")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "1")
                .param("description", "this is the description")
                .param("prepTime", "10")
                .param("cookTime", "20")
                .param("servings", "4"))
                .param("directions", "these are the directions"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/recipes/1"));
    }

    @Test
    @DisplayName("GET /recipes/{id}/update")
    void shouldUpdateExistingEntity() throws Exception {
        var recipeCommand = new RecipeCommand();
        recipeCommand.setId(1L);
        when(recipeService.findCommandById(any()))
                .thenReturn(recipeCommand);

        mockMvc.perform(get("/recipes/{id}/update", 1L))
                .andExpect(status().isOk())
                .andExpect(model().attribute("recipe", recipeCommand))
                .andExpect(view().name("recipe/recipeform"));
    }

    @Test
    @DisplayName("GET /recipes/{id}/delete")
    void shouldDeleteExistingEntity() throws Exception {
        mockMvc.perform(get("/recipes/{id}/delete", 1L))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));

        verify(recipeService).deleteById(anyLong());
    }

}