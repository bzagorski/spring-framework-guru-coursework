package zagsoft.springframework.myspringpetclinic.service.map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import zagsoft.springframework.myspringpetclinic.model.Owner;

import static org.assertj.core.api.Assertions.assertThat;

class OwnerMapServiceTest {

    private OwnerMapService ownerMapService;
    private final long ownerId = 1L;

    @BeforeEach
    void setup() {
        ownerMapService = new OwnerMapService(new PetMapService(), new PetTypeMapService());
        ownerMapService.save(Owner.builder().id(ownerId).build());
    }

    @Test
    void findByLastName() {
        var lastName = "Lebowski";
        var owner = Owner.builder().lastName(lastName).build();
        ownerMapService.save(owner);

        var result = ownerMapService.findByLastName(lastName);

        assertThat(result.getLastName()).isEqualTo(lastName);
    }

    @Test
    void findAll() {
        var result = ownerMapService.findAll();

        assertThat(result).hasSize(1);
    }

    @Test
    void findById() {
        var result = ownerMapService.findById(ownerId);

        assertThat(result.getId()).isEqualTo(ownerId);
    }

    @Test
    void save() {
        var newOwner = Owner.builder().id(2L).build();

        var result = ownerMapService.save(newOwner);

        assertThat(result.getId()).isEqualTo(newOwner.getId());
    }

    @Test
    void saveShouldSetId() {
        var newOwner = Owner.builder().firstName("Jeff").build();

        var result = ownerMapService.save(newOwner);

        assertThat(result.getId()).isNotNull()
                .isEqualTo(2L);
    }

    @Test
    void delete() {
        ownerMapService.delete(ownerMapService.findById(ownerId));

        assertThat(ownerMapService.findAll()).hasSize(0).isEmpty();
    }

    @Test
    void deleteById() {
        ownerMapService.deleteById(ownerId);

        assertThat(ownerMapService.findAll()).isEmpty();
    }
}