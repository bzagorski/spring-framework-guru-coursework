package zagsoft.springframework.myspringpetclinic.service;

import zagsoft.springframework.myspringpetclinic.model.PetType;

public interface PetTypeService extends CrudService<PetType, Long> {
}
