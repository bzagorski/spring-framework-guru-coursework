package zagsoft.springframework.myspringpetclinic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import zagsoft.springframework.myspringpetclinic.model.Vet;
import zagsoft.springframework.myspringpetclinic.service.VetService;

import java.util.Set;

@Controller
public class VetController {

    private final VetService vetService;

    public VetController(VetService vetService) {
        this.vetService = vetService;
    }

    @GetMapping({"/vets", "/vets.html", "/vets/index", "/vets/index.html"})
    public String vets(Model model) {
        model.addAttribute("vets", vetService.findAll());

        return "/vets/index";
    }

    @GetMapping("/api/vets")
    public @ResponseBody Set<Vet> getVetsJson() {
        return vetService.findAll();

    }
}
