package guru.springframework.bootstrap;

import guru.springframework.model.Category;
import guru.springframework.model.UnitOfMeasure;
import guru.springframework.repository.CategoryRepository;
import guru.springframework.repository.UnitOfMeasureRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@Profile({"dev", "prod"})
@RequiredArgsConstructor
public class MySQLDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private final CategoryRepository categoryRepository;
    private final UnitOfMeasureRepository unitOfMeasureRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        if (categoryRepository.count() == 0L) {
            log.debug("Loading categories");
            loadCategories();
        }

        if (unitOfMeasureRepository.count() == 0L) {
            log.debug("Loading UOMs");
            loadUnitOfMeasures();
        }
    }

    private void loadCategories() {
        var categories = List.of(
                new Category("American"),
                new Category("Italian"),
                new Category("Mexican"),
                new Category("Fast Food")
        );
        categoryRepository.saveAll(categories);
    }

    private void loadUnitOfMeasures() {
        var uoms = List.of(
                new UnitOfMeasure("Teaspoon"),
                new UnitOfMeasure("Tablespoon"),
                new UnitOfMeasure("Cup"),
                new UnitOfMeasure("Each")

        );
        unitOfMeasureRepository.saveAll(uoms);
    }
}
