package zagsoft.springframework.myspringpetclinic.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import zagsoft.springframework.myspringpetclinic.model.*;
import zagsoft.springframework.myspringpetclinic.service.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.stream.Stream;

@Component
public class DataLoader implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final SpecialtyService specialtyService;
    private final PetTypeService petTypeMapService;
    private final VisitService visitService;

    public DataLoader(OwnerService ownerService,
                      VetService vetService,
                      SpecialtyService specialtyService,
                      PetTypeService petTypeMapService,
                      VisitService visitService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.specialtyService = specialtyService;
        this.petTypeMapService = petTypeMapService;
        this.visitService = visitService;
    }

    @Override
    public void run(String... args) {
        int count = petTypeMapService.findAll().size();

        if (count == 0) {
            loadData();
        }
    }

    private void loadData() {
        var dog = new PetType("Dog");
        var cat = new PetType("Cat");
        petTypeMapService.save(dog);
        petTypeMapService.save(cat);
        System.out.println(">> Loaded pet types <<");

        var owner1 = Owner.builder()
                .firstName("Michael")
                .lastName("Weston")
                .address("123 Brickerel")
                .city("Miami")
                .telephone("111-111-1111").build();
        var owner2 = Owner.builder()
                .firstName("Fiona")
                .lastName("Glennane")
                .address("123 Brickerel")
                .city("Miami")
                .telephone("111-111-1111").build();
        var owner3 = Owner.builder()
                .firstName("Sam")
                .lastName("Axe").build();

        var mikesPet = new Pet("Rufus", dog, owner1, LocalDate.now());
        owner1.getPets().add(mikesPet);
        var fionasCat = new Pet("Muffins", cat, owner2, LocalDate.now());
        owner2.getPets().add(fionasCat);
        Stream.of(owner1, owner2, owner3)
                .forEach(ownerService::save);
        System.out.println(">> Loaded owners <<");

        var catVisit = new Visit("Sneezy Kitty", LocalDate.now().plusDays(2), fionasCat);
        visitService.save(catVisit);
        System.out.println(">> Loading visits <<");

        var radiology = new Specialty("radiology");
        radiology = specialtyService.save(radiology);
        var surgery = new Specialty("surgery");
        surgery = specialtyService.save(surgery);
        var dentisty = new Specialty("dentistry");
        dentisty = specialtyService.save(dentisty);
        System.out.println(">> Loaded specialties <<");

        var vet1 = Vet.builder().firstName("Jesse").lastName("Porter").build();
        vet1.getSpecialties().add(radiology);
        var vet2 = Vet.builder().firstName("Sam").lastName("Axe").build();
        vet2.getSpecialties().add(surgery);
        vetService.save(vet1);
        vetService.save(vet2);
        System.out.println(">> Loaded vets <<");
    }
}
