package guru.springframework.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@Document
public class Recipe {

    @Id
    private String id;

    private String description;

    private Integer prepTime;

    private Integer cookTime;

    private Integer servings;

    private String source;

    private String url;

    private String directions;

    private Difficulty difficulty;

    private Set<Ingredient> ingredients = new HashSet<>();

    @DBRef
    private Set<Category> categories = new HashSet<>();

    private Byte[] image;

    private Notes notes;

    public Recipe() {
    }

    private Recipe(Builder builder) {
        setId(builder.id);
        setDescription(builder.description);
        setPrepTime(builder.prepTime);
        setCookTime(builder.cookTime);
        setServings(builder.servings);
        setSource(builder.source);
        setUrl(builder.url);
        setDirections(builder.directions);
        setDifficulty(builder.difficulty);
        setIngredients(builder.ingredients);
        setCategories(builder.categories);
        setImage(builder.image);
        setNotes(builder.notes);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(Recipe copy) {
        Builder builder = new Builder();
        builder.id = copy.getId();
        builder.description = copy.getDescription();
        builder.prepTime = copy.getPrepTime();
        builder.cookTime = copy.getCookTime();
        builder.servings = copy.getServings();
        builder.source = copy.getSource();
        builder.url = copy.getUrl();
        builder.directions = copy.getDirections();
        builder.difficulty = copy.getDifficulty();
        builder.ingredients = copy.getIngredients();
        builder.categories = copy.getCategories();
        builder.image = copy.getImage();
        builder.notes = copy.getNotes();
        return builder;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrepTime() {
        return prepTime;
    }

    public void setPrepTime(Integer prepTime) {
        this.prepTime = prepTime;
    }

    public Integer getCookTime() {
        return cookTime;
    }

    public void setCookTime(Integer cookTime) {
        this.cookTime = cookTime;
    }

    public Integer getServings() {
        return servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public Notes getNotes() {
        return notes;
    }

    public void setNotes(Notes notes) {
        if (notes != null) {
            this.notes = notes;
        }
    }

    public Recipe addIngredient(Ingredient ingredient) {
        this.ingredients.add(ingredient);
        return this;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Set<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public static final class Builder {
        private String id;
        private String description;
        private Integer prepTime;
        private Integer cookTime;
        private Integer servings;
        private String source;
        private String url;
        private String directions;
        private Difficulty difficulty;
        private Set<Ingredient> ingredients = new HashSet<>();
        private Set<Category> categories = new HashSet<>();
        private Byte[] image;
        private Notes notes;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder prepTime(Integer val) {
            prepTime = val;
            return this;
        }

        public Builder cookTime(Integer val) {
            cookTime = val;
            return this;
        }

        public Builder servings(Integer val) {
            servings = val;
            return this;
        }

        public Builder source(String val) {
            source = val;
            return this;
        }

        public Builder url(String val) {
            url = val;
            return this;
        }

        public Builder directions(String val) {
            directions = val;
            return this;
        }

        public Builder difficulty(Difficulty val) {
            difficulty = val;
            return this;
        }

        public Builder ingredients(Set<Ingredient> val) {
            ingredients = val;
            return this;
        }

        public Builder categories(Set<Category> val) {
            categories = val;
            return this;
        }

        public Builder image(Byte[] val) {
            image = val;
            return this;
        }

        public Builder notes(Notes val) {
            notes = val;
            return this;
        }

        public Recipe build() {
            return new Recipe(this);
        }
    }
}
