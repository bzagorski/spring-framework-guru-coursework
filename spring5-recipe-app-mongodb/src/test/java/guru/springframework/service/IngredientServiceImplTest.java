package guru.springframework.service;

import guru.springframework.commands.IngredientCommand;
import guru.springframework.converters.IngredientCommandToIngredient;
import guru.springframework.converters.IngredientToIngredientCommand;
import guru.springframework.converters.UnitOfMeasureCommandToUnitOfMeasure;
import guru.springframework.converters.UnitOfMeasureToUnitOfMeasureCommand;
import guru.springframework.model.Ingredient;
import guru.springframework.model.Recipe;
import guru.springframework.repository.RecipeRepository;
import guru.springframework.repository.UnitOfMeasureRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class IngredientServiceImplTest {

    @Mock
    private RecipeRepository recipeRepository;

    @Mock
    private UnitOfMeasureRepository unitOfMeasureRepository;

    private IngredientToIngredientCommand ingredientToIngredientCommand;

    private IngredientCommandToIngredient ingredientCommandToIngredient;

    private IngredientServiceImpl ingredientService;

    @BeforeEach
    void setup() {
        ingredientToIngredientCommand = new IngredientToIngredientCommand(new UnitOfMeasureToUnitOfMeasureCommand());
        ingredientCommandToIngredient = new IngredientCommandToIngredient(new UnitOfMeasureCommandToUnitOfMeasure());
        ingredientService = new IngredientServiceImpl(recipeRepository, unitOfMeasureRepository, ingredientToIngredientCommand, ingredientCommandToIngredient);
    }

    @Test
    @DisplayName("Find ingredient by recipe id and ingredient id")
    void findByRecipeIdAndId() {
        var recipe = Recipe.newBuilder().id("1L").build();
        var ingredient1 = new Ingredient();
        ingredient1.setId("1L");
        var ingredient2 = new Ingredient();
        ingredient2.setId("2L");
        var ingredient3 = new Ingredient();
        ingredient3.setId("3L");
        recipe.getIngredients().addAll(List.of(ingredient1, ingredient2, ingredient3));

        when(recipeRepository.findById(anyString()))
                .thenReturn(Optional.of(recipe));

        var result = ingredientService.findByRecipeIdAndId("1L", "3L");

        assertThat(result.getId()).isEqualTo("3L");
    }

    @Test
    @DisplayName("Save recipe")
    void testSaveRecipeCommand() {
        var ingredientCommand = new IngredientCommand();
        ingredientCommand.setId("3L");
        ingredientCommand.setRecipeId("2L");
        var recipe = new Recipe();
        var ingredient = new Ingredient();
        ingredient.setId("3L");
        recipe.getIngredients().add(ingredient);

        when(recipeRepository.findById(anyString()))
                .thenReturn(Optional.of(new Recipe()));
        when(recipeRepository.save(any()))
                .thenReturn(recipe);

        var result = ingredientService.saveIngredientCommand(ingredientCommand);

        assertThat(result)
                .isNotNull()
                .extracting("id")
                .isEqualTo("3L");
        Mockito.verify(recipeRepository).findById(anyString());
        Mockito.verify(recipeRepository).save(any(Recipe.class));

    }

    @Test
    @DisplayName("Delete Ingredient")
    void testDeleteIngredient() {
        var recipe = new Recipe();
        recipe.setId("1L");
        var ingredient = new Ingredient();
        ingredient.setId("2L");
        recipe.addIngredient(ingredient);

        when(recipeRepository.findById(anyString()))
                .thenReturn(Optional.of(recipe));

        ingredientService.deleteById("1L", "2L");

        verify(recipeRepository).findById("1L");
        verify(recipeRepository).save(any(Recipe.class));
    }
}