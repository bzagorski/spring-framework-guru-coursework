package guru.springframework.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class UnitOfMeasureRepositoryIT {

    @Autowired
    private UnitOfMeasureRepository unitOfMeasureRepository;

    @Test
    @DirtiesContext
        // tells spring this mutates the data
    void findByDescription() {
        var actual = unitOfMeasureRepository.findByDescription("teaspoon");

        assertThat(actual.isPresent()).isTrue();
        assertThat(actual.get().getDescription()).isEqualTo("teaspoon");
    }

    @Test
    void findByDescriptionCup() {
        var cup = unitOfMeasureRepository.findByDescription("cup");

        assertThat(cup.isPresent()).isTrue();
        assertThat(cup.get().getDescription()).isEqualTo("cup");
    }
}