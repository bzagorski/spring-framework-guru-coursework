package guru.springframework.service;

import guru.springframework.converters.UnitOfMeasureToUnitOfMeasureCommand;
import guru.springframework.model.UnitOfMeasure;
import guru.springframework.repository.UnitOfMeasureRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class UnitOfMeasureServiceImplTest {

    private UnitOfMeasureToUnitOfMeasureCommand unitOfMeasureToUnitOfMeasureCommand = new UnitOfMeasureToUnitOfMeasureCommand();

    @Mock
    private UnitOfMeasureRepository unitOfMeasureRepository;

    private UnitOfMeasureService unitOfMeasureService;

    @BeforeEach
    void setup() {
        unitOfMeasureService = new UnitOfMeasureServiceImpl(unitOfMeasureRepository, unitOfMeasureToUnitOfMeasureCommand);
    }

    @Test
    @DisplayName("List all UOMs")
    void listAllUoms() {
        var uom1 = new UnitOfMeasure("cup");
        var uom2 = new UnitOfMeasure("tablespoon");
        var uoms = Set.of(uom1, uom2);

        Mockito.when(unitOfMeasureRepository.findAll()).thenReturn(uoms);

        var result = unitOfMeasureService.listAllUoms();

        assertThat(result)
                .isNotEmpty()
                .hasSize(2)
                .extracting("description")
                .contains("cup", "tablespoon");
    }
}