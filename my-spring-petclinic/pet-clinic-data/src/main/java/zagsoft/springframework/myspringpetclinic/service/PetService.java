package zagsoft.springframework.myspringpetclinic.service;

import zagsoft.springframework.myspringpetclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {
}
