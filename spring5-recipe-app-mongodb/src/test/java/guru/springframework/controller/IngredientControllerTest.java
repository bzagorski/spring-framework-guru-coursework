package guru.springframework.controller;

import guru.springframework.commands.IngredientCommand;
import guru.springframework.commands.RecipeCommand;
import guru.springframework.service.IngredientService;
import guru.springframework.service.RecipeService;
import guru.springframework.service.UnitOfMeasureService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class IngredientControllerTest {

    @Mock
    private RecipeService recipeService;

    @Mock
    private IngredientService ingredientService;

    @Mock
    private UnitOfMeasureService unitOfMeasureService;

    @InjectMocks
    private IngredientController ingredientController;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(ingredientController).build();
    }

    @Test
    @DisplayName("GET /recipes/1/ingredients - SUCCESS")
    void testIngredientList() throws Exception {
        var recipeCommand = new RecipeCommand();
        Mockito.when(recipeService.findCommandById(anyString()))
                .thenReturn(recipeCommand);

        mockMvc.perform(get("/recipes/1/ingredients"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/list"))
                .andExpect(model().attributeExists("recipe"));

        verify(recipeService).findCommandById(anyString());
    }

    @Test
    @DisplayName("GET /recipes/{recipeId}/ingredients/{ingredientId}/show - SUCCESS")
    void testShowIngredient() throws Exception {
        var ingredientCommand = new IngredientCommand();
        when(ingredientService.findByRecipeIdAndId(anyString(), anyString()))
                .thenReturn(ingredientCommand);

        mockMvc.perform(get("/recipes/{recipeId}/ingredients/{ingredientId}/show", 1L, 2L))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/show"))
                .andExpect(model().attributeExists("ingredient"));
    }

    @Test
    @DisplayName("GET /recipes/{recipeId}/ingredients/new - SUCCESS")
    void testNewIngredientForm() throws Exception {
        var recipeCommand = new RecipeCommand();
        recipeCommand.setId("1L");

        when(recipeService.findCommandById(anyString()))
                .thenReturn(recipeCommand);
        when(unitOfMeasureService.listAllUoms())
                .thenReturn(new HashSet<>());

        mockMvc.perform(get("/recipes/{recipeId}/ingredients/new", 1L))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/ingredientform"))
                .andExpect(model().attributeExists("ingredient"))
                .andExpect(model().attributeExists("uomList"));

        verify(recipeService).findCommandById(anyString());
    }

    @Test
    @DisplayName("GET /recipes/{recipeId}/ingredients/{ingredientId}/update - SUCCESS")
    void testUpdateIngredientForm() throws Exception {
        var ingredientCommand = new IngredientCommand();
        ingredientCommand.setId("3L");
        ingredientCommand.setRecipeId("2L");

        when(ingredientService.findByRecipeIdAndId(anyString(), anyString()))
                .thenReturn(ingredientCommand);
        when(unitOfMeasureService.listAllUoms())
                .thenReturn(new HashSet<>());

        mockMvc.perform(get("/recipes/{recipeId}/ingredients/{ingredientId}/update", 2L, 3L))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/ingredient/ingredientform"))
                .andExpect(model().attributeExists("ingredient"))
                .andExpect(model().attributeExists("uomList"));
    }

    @Test
    @DisplayName("POST /recipes/{recipeId}/ingredients - SUCCESS")
    void testSaveOrUpdate() throws Exception {
        var ingredientCommand = new IngredientCommand();
        ingredientCommand.setId("3L");
        ingredientCommand.setRecipeId("2L");

        when(ingredientService.saveIngredientCommand(any()))
                .thenReturn(ingredientCommand);

        mockMvc.perform(post("/recipes/{recipeId}/ingredients", 2L)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "")
                .param("description", "this is the description"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/recipes/2L/ingredients/3L/show"));
    }

    @Test
    @DisplayName("GET /recipes/{recipeId}/ingredients/{ingredientId}/delete - SUCCESS")
    void testDeleteIngredient() throws Exception {
        var recipeId = "1L";
        mockMvc.perform(get("/recipes/{recipeId}/ingredients/{ingredientId}/delete", recipeId, "2L"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/recipes/" + recipeId + "/ingredients"));
        verify(ingredientService).deleteById("1L", "2L");
    }
}