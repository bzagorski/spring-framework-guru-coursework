package guru.springframework.bootstrap;

import guru.springframework.model.*;
import guru.springframework.repository.CategoryRepository;
import guru.springframework.repository.RecipeRepository;
import guru.springframework.repository.UnitOfMeasureRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Component
@Profile("default")
public class DataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private final CategoryRepository categoryRepository;
    private final UnitOfMeasureRepository unitOfMeasureRepository;
    private final RecipeRepository recipeRepository;

    public DataLoader(CategoryRepository categoryRepository, UnitOfMeasureRepository unitOfMeasureRepository, RecipeRepository recipeRepository) {
        this.categoryRepository = categoryRepository;
        this.unitOfMeasureRepository = unitOfMeasureRepository;
        this.recipeRepository = recipeRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        recipeRepository.saveAll(getRecipes());
    }

    public List<Recipe> getRecipes() {
        var teaspoon = unitOfMeasureRepository.findByDescription("teaspoon")
                .orElseThrow(() -> new RuntimeException("No uom found with description: teaspoon"));
        var tablespoon = unitOfMeasureRepository.findByDescription("tablespoon")
                .orElseThrow(() -> new RuntimeException("No uom found with description: tablespoon"));
        var cup = unitOfMeasureRepository.findByDescription("cup")
                .orElseThrow(() -> new RuntimeException("No uom found with description: cup"));
        var pinch = unitOfMeasureRepository.findByDescription("pinch")
                .orElseThrow(() -> new RuntimeException("No uom found with description: pinch"));
        var pint = unitOfMeasureRepository.findByDescription("pint")
                .orElseThrow(() -> new RuntimeException("No uom found with description: pint"));
        var ounce = unitOfMeasureRepository.findByDescription("ounce")
                .orElseThrow(() -> new RuntimeException("No uom found with description: ounce"));
        var dash = unitOfMeasureRepository.findByDescription("dash")
                .orElseThrow(() -> new RuntimeException("No uom found with description: dash"));
        var each = unitOfMeasureRepository.findByDescription("each")
                .orElseThrow(() -> new RuntimeException("No uom found with description: each"));

        var american = categoryRepository.findByDescription("American")
                .orElseThrow(() -> new RuntimeException("No category found with description: American"));
        var mexican = categoryRepository.findByDescription("Mexican")
                .orElseThrow(() -> new RuntimeException("No category found with description: Mexican"));

        var guacamole = Recipe.newBuilder()
                .description("How to Make Perfect Guacamole Recipe")
                .source("Simply Recipes")
                .url("https://www.simplyrecipes.com/recipes/perfect_guacamole/")
                .prepTime(10)
                .cookTime(30)
                .servings(4)
                .difficulty(Difficulty.EASY)
                .categories(Set.of(categoryRepository.findByDescription("Mexican").orElse(new Category("N/A"))))
                .directions("1 Cut the avocado, remove flesh: Cut the avocados in half. Remove the pit. Score the inside of the avocado with a blunt knife and scoop out the flesh with a spoon. (See How to Cut and Peel an Avocado.) Place in a bowl.\n" +
                        "\n" +
                        "2 Mash with a fork: Using a fork, roughly mash the avocado. (Don't overdo it! The guacamole should be a little chunky.)\n" +
                        "\n" +
                        "3 Add salt, lime juice, and the rest: Sprinkle with salt and lime (or lemon) juice. The acid in the lime juice will provide some balance to the richness of the avocado and will help delay the avocados from turning brown.\n" +
                        "\n" +
                        "Add the chopped onion, cilantro, black pepper, and chiles. Chili peppers vary individually in their hotness. So, start with a half of one chili pepper and add to the guacamole to your desired degree of hotness.\n" +
                        "\n" +
                        "Remember that much of this is done to taste because of the variability in the fresh ingredients. Start with this recipe and adjust to your taste.\n" +
                        "\n" +
                        "Chilling tomatoes hurts their flavor, so if you want to add chopped tomato to your guacamole, add it just before serving.\n" +
                        "\n" +
                        "4 Serve: Serve immediately, or if making a few hours ahead, place plastic wrap on the surface of the guacamole and press down to cover it and to prevent air reaching it. (The oxygen in the air causes oxidation which will turn the guacamole brown.) Refrigerate until ready to serve.\n")
                .build();
        guacamole.setNotes(new Notes("Be careful handling chiles if using. Wash your hands thoroughly after handling and do not touch your eyes or the area near your eyes with your hands for several hours.", guacamole));
        guacamole
                .addIngredient(new Ingredient("ripe avocados", BigDecimal.valueOf(2), each))
                .addIngredient(new Ingredient("salt", BigDecimal.valueOf(0.25), teaspoon))
                .addIngredient(new Ingredient("fresh lime or lemon juice", BigDecimal.ONE, tablespoon))
                .addIngredient(new Ingredient("minced red onion or thinly sliced green onion", BigDecimal.valueOf(2), tablespoon))
                .addIngredient(new Ingredient("serrano chiles, stems and seeds removed, minced", BigDecimal.valueOf(2), each))
                .addIngredient(new Ingredient("cilantro (leaves and tender stems), finely chopped", BigDecimal.valueOf(2), tablespoon))
                .addIngredient(new Ingredient("freshly grated black pepper", BigDecimal.ONE, dash))
                .addIngredient(new Ingredient("ripe tomato, seeds and pulp removed, chopped", BigDecimal.ONE, each))
                .addIngredient(new Ingredient("Red radishes or jicama, to garnish"))
                .addIngredient(new Ingredient("Tortilla chips, to serve"));

        var tacos = Recipe.newBuilder()
                .description("Spicy Grilled Chicken Tacos")
                .source("Simply Recipes")
                .url("https://www.simplyrecipes.com/recipes/spicy_grilled_chicken_tacos/")
                .prepTime(20)
                .cookTime(10)
                .servings(6)
                .difficulty(Difficulty.MODERATE)
                .categories(Set.of(
                        categoryRepository.findByDescription("Mexican").orElse(new Category("N/A")),
                        categoryRepository.findByDescription("American").orElse(new Category("N/A"))
                ))
                .directions("1 Prepare a gas or charcoal grill for medium-high, direct heat.\n" +
                        "\n" +
                        "2 Make the marinade and coat the chicken: In a large bowl, stir together the chili powder, oregano, cumin, sugar, salt, garlic and orange zest. Stir in the orange juice and olive oil to make a loose paste. Add the chicken to the bowl and toss to coat all over.\n" +
                        "\n" +
                        "Set aside to marinate while the grill heats and you prepare the rest of the toppings.\n" +
                        "\n" +
                        "3 Grill the chicken: Grill the chicken for 3 to 4 minutes per side, or until a thermometer inserted into the thickest part of the meat registers 165F. Transfer to a plate and rest for 5 minutes.\n" +
                        "\n" +
                        "4 Warm the tortillas: Place each tortilla on the grill or on a hot, dry skillet over medium-high heat. As soon as you see pockets of the air start to puff up in the tortilla, turn it with tongs and heat for a few seconds on the other side.\n" +
                        "\n" +
                        "Wrap warmed tortillas in a tea towel to keep them warm until serving.\n" +
                        "\n" +
                        "5 Assemble the tacos: Slice the chicken into strips. On each tortilla, place a small handful of arugula. Top with chicken slices, sliced avocado, radishes, tomatoes, and onion slices. Drizzle with the thinned sour cream. Serve with lime wedges.\n")
                .build();
        tacos.setNotes(new Notes("Look for ancho chile powder with the Mexican ingredients at your grocery store, on buy it online. (If you can't find ancho chili powder, you replace the ancho chili, the oregano, and the cumin with 2 1/2 tablespoons regular chili powder, though the flavor won't be quite the same.)", tacos));
        tacos
                .addIngredient(new Ingredient("ancho chili powder", BigDecimal.valueOf(2), tablespoon))
                .addIngredient(new Ingredient("dried oregano", BigDecimal.ONE, teaspoon))
                .addIngredient(new Ingredient("dried cumin", BigDecimal.ONE, teaspoon))
                .addIngredient(new Ingredient("sugar", BigDecimal.ONE, teaspoon))
                .addIngredient(new Ingredient("salt", BigDecimal.valueOf(0.5), teaspoon))
                .addIngredient(new Ingredient("clove garlic, finely chopped", BigDecimal.ONE, each))
                .addIngredient(new Ingredient("finely grated orange zest", BigDecimal.ONE, tablespoon))
                .addIngredient(new Ingredient("fresh-squeezed orange juice", BigDecimal.valueOf(3), tablespoon))
                .addIngredient(new Ingredient("olive oil", BigDecimal.valueOf(2), tablespoon))
                .addIngredient(new Ingredient("boneless skinless chicken thighs", BigDecimal.valueOf(4), each))
                .addIngredient(new Ingredient("small corn tortillas", BigDecimal.valueOf(8), each))
                .addIngredient(new Ingredient("packed baby arugula (3 ounces)", BigDecimal.valueOf(3), cup))
                .addIngredient(new Ingredient("medium ripe avocados, sliced", BigDecimal.valueOf(2), each))
                .addIngredient(new Ingredient("radishes, thinly sliced", BigDecimal.valueOf(4), each))
                .addIngredient(new Ingredient("cherry tomatoes, halved", BigDecimal.valueOf(0.5), pint))
                .addIngredient(new Ingredient("red onion, thinly sliced", BigDecimal.valueOf(0.25), each))
                .addIngredient(new Ingredient("Roughly chopped cilantro"))
                .addIngredient(new Ingredient("sour cream", BigDecimal.valueOf(0.5), cup))
                .addIngredient(new Ingredient("lime", BigDecimal.ONE, each));

        return List.of(guacamole, tacos);
    }
}
